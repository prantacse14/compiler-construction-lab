<b> North East University Bangladesh </b> <br/>
CSE 422 - Compiler Construction Lab <br/>
Lab Exam - 1 | Marks: 30 <br/>
Time: 1.30 hours


<br/>

<b> Please follow the instructions below: </b> <br/>
1.  At first download the text file which name is **"input.txt"**.
2.  Solve your desired problem included in the text file.
3.  Make a file name which is following the format : `id_firstname_compiler_lab_1.cpp`
4.  Go to your mail and write subject as : *"Compiler Lab Exam-1"*
5.  Finally, send your solution (.cpp) file to the mail address : **`prantacse04@gmail.com`**